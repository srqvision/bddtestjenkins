package com.co.jenkinsTest.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.steps.StepInterceptor;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.LoggerFactory;
import java.util.concurrent.TimeUnit;


@DefaultUrl("https://dllocoreseguros.suramericana.com.co/gc-dashboard/")
public class AutenticationGCPage extends PageObject{

    private static final int ESPERAVISIBILIDAD = 3;
    public static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(StepInterceptor.class);
    @FindBy(id = "suranetName")
    WebElementFacade txtUsuario;
    @FindBy(id = "suranetPassword")
    WebElementFacade txtContrasena;
    @FindBy(xpath = "//grid-dashboard/p-datatable//table/thead/tr")
    WebElementFacade tblCabeceraTabla;
    @FindBy(xpath = "//a[contains(.,'Empleado')]")
    WebElementFacade tabEmpleado;
    @FindBy(xpath = "//app-navbar/div/div/div/ul/li/img")
    WebElementFacade imgUsuario;
    @FindBy(xpath = "//a[contains(.,'Salir')]")
    WebElementFacade btnSalir;

    private void esperarPorVisibilidad(WebElementFacade elementoAEsperar) {
        try {
            elementoAEsperar.setImplicitTimeout(new Duration(ESPERAVISIBILIDAD, TimeUnit.SECONDS));
            waitFor(ExpectedConditions.visibilityOf(elementoAEsperar));
        } catch (Exception ex) {
            LOGGER.info("El campo no estuvo visible" + ex);
        }
        resetImplicitTimeout();
    }

    private void diligenciarIngreso(String usr, String psw){
        try {
            txtUsuario.waitUntilVisible()
                    .waitUntilClickable()
                    .type(usr);
            txtContrasena.waitUntilVisible().type(psw).sendKeys(Keys.ENTER);
        } catch (Exception e){
            LOGGER.error("No se encuentra el elemento: " + e);
        }
    }

    public void ingresoUsuario(String usr, String psw) {
        setImplicitTimeout(2, TimeUnit.SECONDS);
        if (!tblCabeceraTabla.isPresent()) {
            this.getDriver().manage().deleteAllCookies();
            esperarPorVisibilidad(tabEmpleado);
            if(tabEmpleado.isVisible()){
                tabEmpleado.click();
            }
            this.diligenciarIngreso(usr,psw);
        }
        resetImplicitTimeout();
    }

    public void ingresarDashBoard() {
        tblCabeceraTabla.waitUntilPresent();
        MatcherAssert.assertThat("No se cargaron los datos de la tabla", tblCabeceraTabla.isPresent());
    }

    public void cerrarSesionActual(){
        Actions actions = new Actions(this.getDriver());
        actions.moveToElement(imgUsuario).perform();
        btnSalir.click();
    }

}
