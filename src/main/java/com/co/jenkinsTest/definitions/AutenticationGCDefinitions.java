package com.co.jenkinsTest.definitions;

import com.co.jenkinsTest.steps.AutenticationGCSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Then;

public class AutenticationGCDefinitions {

    @Steps
    private AutenticationGCSteps autenticationGCSteps;

    @Then("debe permitirme ingresar al dash board")
    public void thenDebePermitirmeIngresarAlDashBoard() {
        autenticationGCSteps.abrirAplicacionDashBoard("carlpase", "carlpase");
    }

    @Then("debe permitirme cerrar sesion")
    public void thenDebePermitirmeCerrarSesion() {
        autenticationGCSteps.cerrarAplicación();
    }
}
