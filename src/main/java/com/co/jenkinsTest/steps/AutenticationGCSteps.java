package com.co.jenkinsTest.steps;

import com.co.jenkinsTest.pages.AutenticationGCPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.jbehave.core.model.ExamplesTable;

public class AutenticationGCSteps extends ScenarioSteps{

    AutenticationGCPage autenticationGCPage;

    @Step
    public void abrirAplicacionDashBoard(String usr, String psw) {
        autenticationGCPage.open();
        autenticationGCPage.ingresoUsuario(usr, psw);
        autenticationGCPage.ingresarDashBoard();
    }
    @Step
    public void loginGroup(String usr, String psw) {
        autenticationGCPage.ingresoUsuario(usr, psw);
        autenticationGCPage.ingresarDashBoard();
    }

    @Step
    public void cerrarAplicación(){
        autenticationGCPage.cerrarSesionActual();
    }
}
